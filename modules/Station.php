<?php
class Station {
  public $id;
  public $name;
  public $ip;
  public $port;
  public $board;

  public function checkAvailability(){
    return @fsockopen($this->ip,$this->port,$errno,$errstr,1) ? true : false;
  }

  public function new(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $addStation = $pdo->query("INSERT INTO stations(name,ip,port,board) VALUES('".htmlspecialchars($this->name)."','".htmlspecialchars($this->ip)."','".htmlspecialchars($this->port)."','".htmlspecialchars($this->board)."')");
    return $addStation ? true : false;
  }

  public function update(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $updateStation = $pdo->query("UPDATE stations SET name='".htmlspecialchars($this->name)."',ip='".htmlspecialchars($this->ip)."',port='".htmlspecialchars($this->port)."' WHERE id='".htmlspecialchars($this->id)."'");
    return $updateStation ? true : false;
  }

  public function delete(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $deleteStation = $pdo->query("DELETE FROM stations WHERE id='".htmlspecialchars($this->id)."'");
    $deleteModules = $pdo->query("DELETE FROM modules WHERE station='".htmlspecialchars($this->id)."'");
    return $deleteStation && $deleteModules ? true : false;
  }

  public function modulesCount(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT * FROM modules WHERE station='".htmlspecialchars($this->id)."'");
    return $sql->rowCount();
  }

  public function boardName(){
    $Database = Database::getInstance();
    $pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT name FROM boards WHERE id='".htmlspecialchars($this->board)."'");
    return $sql->fetch()['name'];
  }

}

?>
