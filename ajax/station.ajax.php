<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$response = array(
  'emptyData'=>array(
    'type'=>'error',
    'message'=>'Empty data.',
    'elements'=>'all'
  ),
  'error'=>array(
    'type'=>'error',
    'message'=>'Unkown error.',
    'elements'=>'all'
  ),
  'success'=>array(
    'type'=>'success',
    'message'=>'All ok.',
    'elements'=>'all'
  ),
  'busyName'=>array(
    'type'=>'error',
    'message'=>'Name is now using by other module.',
    'elements'=>'name'
  ),
  'busyIP'=>array(
    'type'=>'error',
    'message'=>'Other module works on this IP address.',
    'elements'=>'ip'
  ),
  'notAvailability'=>array(
    'type'=>'error',
    'message'=>'Server dont responding on this address.',
    'elements'=>'ip,port'
  )
);

DEFINE('WHOIS','SYSTEM');
chdir("../");
include('config.php');

if($_POST['action'] == 'add'){
  if($_POST['name'] != "" && $_POST['ip'] != "" && $_POST['port'] != "" && $_POST['board'] != ""){
    $Database = Database::getInstance();
    $pdo = $Database->getPDO();
    $checkName = $pdo->query("SELECT id FROM stations WHERE name='".htmlspecialchars($_POST['name'])."'");
    if($checkName->rowCount() > 0){
      echo json_encode($response['busyName']);
    } else {
      $checkIP = $pdo->query("SELECT id FROM stations WHERE ip='".htmlspecialchars($_POST['ip'])."'");
      if($checkIP->rowCount() > 0){
        echo json_encode($response['busyIP']);
      } else {
        $Station = new Station;
        $Station->name = $_POST['name'];
        $Station->ip = $_POST['ip'];
        $Station->port = $_POST['port'];
        $Station->board = $_POST['board'];
        if($Station->checkAvailability()){
          if($Station->new()){
            echo json_encode($response['success']);
          } else echo json_encode($response['error']);
        } else echo json_encode($response['notAvailability']);
      }
      $checkIP->closeCursor();
    }
    $checkName->closeCursor();
  } else echo json_encode($response['emptyData']);
} elseif($_POST['action'] == 'edit'){
  if($_POST['name'] != "" && $_POST['ip'] != "" && $_POST['port'] != ""){
    $Database = Database::getInstance();
    $pdo = $Database->getPDO();
    $checkName = $pdo->query("SELECT id FROM stations WHERE name='".htmlspecialchars($_POST['name'])."'");
    if($checkName->rowCount() > 0 AND $checkName->fetch()['id'] != $_POST['id']){
      echo json_encode($response['busyName']);
    } else {
      $checkIP = $pdo->query("SELECT id FROM stations WHERE ip='".htmlspecialchars($_POST['ip'])."'");
      if($checkIP->rowCount() > 0 AND $checkIP->fetch()['id'] != $_POST['id']){
        echo json_encode($response['busyIP']);
      } else {
        $Station = new Station;
        $Station->id = $_POST['id'];
        $Station->name = $_POST['name'];
        $Station->ip = $_POST['ip'];
        $Station->port = $_POST['port'];
        if($Station->checkAvailability()){
          if($Station->update()){
            echo json_encode($response['success']);
          } else echo json_encode($response['error']);
        } else echo json_encode($response['notAvailability']);
      }
      $checkIP->closeCursor();
    }
    $checkName->closeCursor();
  } else echo json_encode($response['emptyData']);
} elseif($_POST['action'] == 'delete'){
  if($_POST['id'] != ''){
      $Station = new Station;
      $Station->id = $_POST['id'];
      if($Station->delete()) echo json_encode($response['success']);
      else echo json_encode($response['error']);
  } else echo json_encode($response['emptyData']);
} else echo json_encode($response['error']);

?>
