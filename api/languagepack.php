<?php
DEFINE('WHOIS','ESP');
chdir("../");
include('config.php');
header('Content-Type: application/json');
if($_GET['language'] != ""){
  $lang = $_GET['language'];
  $Database = Database::getInstance();
  $pdo = $Database->getPDO();
  $sql = $pdo->query("SELECT * FROM languages");
  $settings = array();
  foreach($sql as $row) $settings[$row['name']] = $row[$lang];
  echo json_encode($settings,JSON_PRETTY_PRINT);
}

?>
