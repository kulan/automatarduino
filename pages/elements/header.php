<div class="col-sm-12 col-md-4 col-lg-5">
  <div class="card large">
    <div class="section">
      <h1><?php echo PAGE_NAME; ?></h1>
    </div>
  </div>
</div>
<div class="col-sm-12 col-md-4 col-lg-3">
  <div class="card large">
    <div id="login-info"></div>
    <form method="" id="login-form">
      <div class="input-group fluid">
        <label for="login-username"><span class="icon-user"></span></label>
        <input type="text" id="login-username" placeholder="Twoja nazwa uÅ¼ytkownika"/>
      </div>
      <div class="input-group fluid">
        <label for="login-password"><span class="icon-lock"></span></label>
        <input type="password" id="login-password" placeholder="Twoje hasÅo do konta"/>
      </div>
    </form>
    <div class="button-group">
      <label class="button inverse" for="register-modal">Rejestracja</label>
      <button class="primary" onClick="login();">Zaloguj siÄ!</button>
    </div>
  </div>
  <div class="card large warning">
    <div class="section --fore-green">
      <h3>LINKI</h3>
      <a href="home">STRONA GÅÃWNA</a>
      <a href="activate">AKTYWACJA</a>
    </div>
  </div>
</div>
