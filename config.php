<?php

define('PAGE_URL','http://localhost:8888'); //must be with protocol (HTTP or HTTPS)
define('PAGE_NAME','ZARZÄDZANIE BUDYNKIEM');

//mysql settings
DEFINE('DB_HOST','localhost');
DEFINE('DB_USER','root');
DEFINE('DB_PASSWORD','root');
DEFINE('DB_NAME','house');

DEFINE('CLASS_PATH',PAGE_URL.'/modules/');

function autoload($className, $directory = "modules") {
	$dir = opendir($directory);
	while (($dirname = readdir($dir)) != null) {
		if ($dirname[0] != ".") { // don't care about hidden/special files (.gitignore, .., .)

			if (is_dir("$directory/$dirname")) autoload($className, "$directory/$dirname");
			else if (is_file("$directory/$dirname")) {
				if(preg_match("/(.*)\\.php$/", $dirname, $match)) {
					if($match[1] === $className) {
						include("$directory/$dirname");
					}
				}
			}
		}
	}
	closedir($dir);
}
spl_autoload_register(function($className) { autoload($className); } );



spl_autoload_register('autoload');
?>
